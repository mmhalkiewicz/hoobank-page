import React from 'react';
import Discount from '../assets/Discount.svg';
import { BsArrowUpRight } from 'react-icons/bs';
import { motion } from 'framer-motion';

const Hero = () => {
  return (
    <div className="relative">
      <div>
        <motion.div
          initial={{ opacity: 0, scale: 0.5 }}
          whileInView={{ opacity: 1 }}
          animate={{ scale: 1 }}
          transition={{ duration: 0.5 }}
          className="mx-5 lg:mx-0">
          <div className="flex items-center px-4 py-1.5 rounded-[10px] bg-gradient-to-r from-[#272727] to-[#11101D] max-w-[408px] mb-3">
            <img src={Discount} alt="Discount image" />
            <p className="text-sm text-white lg:text-[18px]">
              20% <span className="text-opacity-60">DISCOUNT FOR</span> 1 MONTH{' '}
              <span className="text-opacity-60">ACCOUNT</span>
            </p>
          </div>
          <div className="text-white max-w-[670px]">
            <h1 className=" text-3xl lg:text-7xl  lg:leading-[100.8px] mb-7">
              The Next{' '}
              <span className="text-transparent bg-clip-text colorful-text">Generation</span>{' '}
              Payment Method.
            </h1>
            <p className="text-white text-opacity-70 lg:text-[18px] lg:leading-[30.6px] max-w-[483px]">
              Our team of experts uses a methodology to identify the credit cards most likely to fit
              your needs. We examine annual percentage rates, annual fees.
            </p>
          </div>
        </motion.div>
      </div>
      <motion.div
        initial={{ opacity: 0, scale: 0.5 }}
        animate={{ opacity: 1, scale: 1 }}
        transition={{ duration: 0.5 }}
        className="hidden lg:flex items-center justify-center absolute top-0 right-0 text-white w-[140px] h-[140px]  rounded-full  colorful-border ">
        <div className="flex flex-col  items-center justify-center rounded-full w-[136px] h-[136px] bg-[#00040f]">
          <div className="flex items-center">
            <span className="colorful-text tex">Get</span>
            <BsArrowUpRight className="ml-3" />
          </div>
          <span className="colorful-text "> Started</span>
        </div>
      </motion.div>
    </div>
  );
};

export default Hero;
