import React from 'react';
import Robot from '../assets/robot.png';
import propTypes from 'prop-types';

import { motion } from 'framer-motion';

const Illustration = ({ className }) => {
  return (
    <div className={className}>
      <motion.div
        initial={{ opacity: 0, scale: 0.5 }}
        whileInView={{ opacity: 1 }}
        animate={{ scale: 1 }}
        transition={{ duration: 0.5 }}
        className="absolute z-30">
        <img src={Robot} alt="Robot image" className="max-w-[669px]" />
      </motion.div>
      <div className="hand-gradient w-[578px] h-[571px] filter blur-3xl opacity-60 relative">
        <br />
      </div>
    </div>
  );
};

Illustration.propTypes = {
  className: propTypes.string.isRequired
};
export default Illustration;
