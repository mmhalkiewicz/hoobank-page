import React from 'react';
import billImage from '../assets/bill.png';
import appStoreImage from '../assets/app-store.svg';
import googlePlayImage from '../assets/google-play.svg';
import Button from './Button';
import StatsImage from '../assets/cards.png';
import propTypes from 'prop-types';
import { motion } from 'framer-motion';

const HeroThird = ({ className }) => {
  return (
    <section className={`mx-5 md:mx-auto md:max-w-screen-md lg:max-w-screen-xl  ${className}`}>
      <div className="lg:flex flex-row items-center justify-center gap-x-[170px] lg:mb-[180px]">
        <motion.div
          initial={{ opacity: 0 }}
          whileInView={{ opacity: 1 }}
          transition={{ delay: 0.5 }}
          className="mb-10 max-w-[530px]">
          <img src={billImage} alt="Bill illustration" />
        </motion.div>
        <motion.div
          initial={{ opacity: 0, x: 200 }}
          whileInView={{ opacity: 1, x: 0 }}
          transition={{ delay: 0.5 }}>
          <h1 className="text-3xl lg:text-5xl lg:leading-[76.8px] text-white mb-4 lg:mb-6 max-w-[470px]">
            Easily control your billing & invoicing.
          </h1>
          <p className="text-opacity-70 text-white max-w-[470px] mb-10 lg:mb-12">
            Elit enim sed massa etiam. Mauris eu adipiscing ultrices ametodio aenean neque. Fusce
            ipsum orci rhoncus aliporttitor integer platea placerat.
          </p>
          <div className="flex items-center  mb-32 lg:mb-0">
            <div className="mr-[32px]">
              <img src={googlePlayImage} alt="Google play image" />
            </div>
            <div>
              <img src={appStoreImage} alt="App store image" />
            </div>
          </div>
        </motion.div>
      </div>
      <div className="lg:flex items-center justify-center gap-x-[80px]">
        <motion.div
          initial={{ opacity: 0, x: -200 }}
          whileInView={{ opacity: 1, x: 0 }}
          transition={{ delay: 0.5 }}
          className="mb-10 lg:mb-0">
          <h1 className="text-3xl lg:text-5xl lg:leading-[76.8px] text-white mb-4 lg:mb-6 max-w-[570px]">
            Find a better card deal in few easy steps.
          </h1>
          <p className="text-opacity-70 text-white max-w-[570px] mb-4 lg:mb-12 max-w-[500px]">
            Arcu tortor, purus in mattis at sed integer faucibus. Aliquet quis aliquet eget mauris
            tortor.ç Aliquet ultrices ac, ametau.
          </p>
          <Button />
        </motion.div>
        <motion.div
          initial={{ opacity: 0 }}
          whileInView={{ opacity: 1 }}
          transition={{ delay: 0.5 }}
          className="max-w-[513px]">
          <img src={StatsImage} alt="Stats image" />
        </motion.div>
      </div>
    </section>
  );
};

HeroThird.propTypes = {
  className: propTypes.string.isRequired
};

export default HeroThird;
