import React from 'react';
import propTypes from 'prop-types';

const LinkList = (props) => {
  const list = props.list;
  const listLinks = list.links;
  const style = props.className;
  return (
    <ul className={`flex flex-col gap-y-2 mb-10 ${style}`}>
      <h1 className="text-white font-medium lg:mb-[24px]">{list.title}</h1>
      {listLinks.map((item) => (
        <li
          key={item}
          className="flex flex-col gap-y-6 lg:gap-y-3 text-white font-normal text-opacity-70 hover:text-opacity-100 ease-in-out duration-500 cursor-pointer ">
          {item}
        </li>
      ))}
    </ul>
  );
};

LinkList.propTypes = {
  list: propTypes.object.isRequired,
  className: propTypes.string.isRequired
};
export default LinkList;
