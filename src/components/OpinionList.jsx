import React from 'react';
import Avatar1 from '../assets/avatar1.png';
import Avatar2 from '../assets/avatar2.png';
import Avatar3 from '../assets/avatar3.png';
import OpinionItem from './OpinionItem';

const OpinionList = () => {
  const list = [
    {
      id: 1,
      img: Avatar1,
      userName: 'Herman Jensen',
      jobTitle: 'Founder & Leader',
      opinion:
        'Money is only a tool. It will take you wherever you wish, but it will not replace you as the driver.',
      text: 'The best credit cards offer some tantalizing combinations of promotions and prizes'
    },
    {
      id: 2,
      img: Avatar2,
      userName: 'Steve Mark',
      jobTitle: 'Founder & Leader',
      opinion: "Money makes your life easier. If you're lucky to have it, you're lucky."
    },
    {
      id: 3,
      img: Avatar3,
      userName: 'Kenn Gallagher',
      jobTitle: 'Founder & Leader',
      opinion:
        'It is usually people in the money business, finance, and international trade that are really rich.'
    }
  ];
  return (
    <div className="flex flex-col md:mx-auto items-center justify-center lg:flex-row gap-y-20 lg:gap-y-0 lg:gap-x-[70px]">
      <OpinionItem list={list} />
    </div>
  );
};

export default OpinionList;
