import React from 'react';
import smallLogo from '../assets/logo-small.svg';
import { FiMenu } from 'react-icons/fi';
import DesktopMenu from './DesktopMenu';

const Navigation = () => {
  return (
    <div>
      <div className="flex items-center p-4 py-9 md:mx-auto md:max-w-screen-md lg:max-w-screen-xl">
        <div className="flex items-center">
          <img src={smallLogo} alt="Logo image" className="mr-3" />
          <div className="text-lg font-bold">
            <span className="text-white">Hoo</span>
            <span className="text-[#5CE1E6]">Bank</span>
          </div>
        </div>
        <div className="ml-auto lg:hidden">
          <FiMenu size={30} color={'white'} />
        </div>
        <DesktopMenu className="hidden lg:block lg:ml-auto " />
      </div>
    </div>
  );
};

export default Navigation;
