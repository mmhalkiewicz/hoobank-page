import React from 'react';
import Button from './Button';
import propTypes from 'prop-types';
import { motion } from 'framer-motion';

const NewsletterComponent = ({ className }) => {
  return (
    <section className={`md:mx-auto md:max-w-screen-md lg:max-w-screen-xl ${className}`}>
      <motion.div
        initial={{ opacity: 0 }}
        whileInView={{ opacity: 1 }}
        transition={{ delay: 0.5 }}
        className="lg:flex items-center  gap-x-[133px]  black-light-gradient rounded-[20px] mx-5 py-12 lg:py-[72px] px-3 lg:px-[97px] max-w-[1170px]">
        <div>
          <h1 className="text-3xl lg:text-5xl font-semibold lg:leading-[67.2px] text-white mb-4 lg:mb-6 max-w-[670px]">
            Let’s try our service now!
          </h1>
          <p className="text-opacity-70 lg:text-[18px] text-white max-w-[445px] mb-10 lg:mb-0">
            Everything you need to accept card payments and grow your business anywhere on the
            planet.
          </p>
        </div>
        <div className="lg:ml-[3px]">
          <Button />
        </div>
      </motion.div>
    </section>
  );
};

NewsletterComponent.propTypes = {
  className: propTypes.string.isRequired
};

export default NewsletterComponent;
