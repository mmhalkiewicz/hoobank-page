import React from 'react';

const Button = () => {
  return (
    <button className=" text-black rounded-md py-4 px-8 lg:text-lg font-medium lg:py-[19px] lg:px-[33px] hover:scale-110 duration-500 ease-in-out">
      Get Started
    </button>
  );
};

export default Button;
