import React from 'react';

const ProsItem = (props) => {
  const list = props.list;
  return list.map((item) => (
    <div
      key={item.id}
      className="flex items-center text-white lg:even:after:w-1 lg:even:after:h-[13.63px] lg:even:after:bg-white lg:even:after:mx-12 lg:even:before:mx-12 lg:even:before:w-1  lg:even:before:h-[13.63px] lg:even:before:bg-white">
      <p className="mr-2 lg:mr-[29.97px] font-bold lg:text-[40.89px]">{item.number}</p>
      <p className="colorful-text font-thin uppercase lg:text-[20.45px]">{item.text}</p>
    </div>
  ));
};

export default ProsItem;
