import React from 'react';
import propTypes from 'prop-types';
import quoteLight from '../assets/“.svg';
import quoteDark from '../assets/“(1).svg';

const OpinionItem = (props) => {
  const list = props.list;
  return list.map((item) => (
    <div
      className=" group black-light-gradient-hover max-w-[370px] h-[395px]  px-[20px] py-[30px] lg:px-[40px] lg:py-[61px] rounded-[20px] cursor-pointer hover:scale-105 ease-in-out duration-500"
      key={item.id}>
      <img src={quoteLight} alt="" className="hidden group-hover:block mb-[40px]" />
      <img src={quoteDark} alt="" className=" group-hover:hidden mb-[40px]" />
      <p className="leading-[32.4px] lg:text-[18px] text-white mb-[56px]">{item.opinion}</p>
      <div className="flex items-center">
        <div>
          <img src={item.img} alt="User image" />
        </div>
        <div className="ml-[16px]">
          <h4 className="text-white text-[20px]">{item.userName}</h4>
          <h6 className="text-white text-opacity-50">{item.jobTitle}</h6>
        </div>
      </div>
    </div>
  ));
};

OpinionItem.propType = {
  list: propTypes.array.isRequired
};
export default OpinionItem;
