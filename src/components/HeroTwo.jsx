import React from 'react';
import Button from './Button';
import AwardsList from './AwardsList';
import propTypes from 'prop-types';

import { motion } from 'framer-motion';

const HeroTwo = ({ className }) => {
  return (
    <div
      className={`mx-4 lg:flex lg:flex-row  gap-x-[78px] justify-center md:mx-auto md:max-w-screen-md lg:max-w-screen-xl ${className}`}>
      <motion.div
        initial={{ opacity: 0, x: -200 }}
        whileInView={{ opacity: 1, x: 0 }}
        transition={{ delay: 0.5 }}
        className="mb-32 lg:mb-0 max-w-[622px]">
        <h1 className="text-3xl lg:text-5xl lg:leading-[76.8px] font-semibold text-white mb-4 lg:mb-6">
          You do the business, we’ll handle the money.
        </h1>
        <p className="text-opacity-70 text-white max-w-[570px] mb-4 lg:mb-12">
          With the right credit card, you can improve your financial life by building credit,
          earning rewards and saving money. But with hundreds of credit cards on the market.
        </p>
        <Button />
      </motion.div>
      <div>
        <AwardsList />
      </div>
    </div>
  );
};

HeroTwo.propTypes = {
  className: propTypes.string.isRequired
};

export default HeroTwo;
