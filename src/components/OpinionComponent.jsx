import React from 'react';
import OpinionList from './OpinionList';
import propTypes from 'prop-types';
import AirbnbImage from '../assets/airbnb.png';
import BinanceImage from '../assets/binance.png';
import CoinbaseImage from '../assets/coinbase.png';
import DropboxImage from '../assets/dropbox.png';
import { motion } from 'framer-motion';

const OpinionComponent = ({ className }) => {
  return (
    <section className={`mx-5 md:mx-auto md:max-w-screen-md lg:max-w-screen-xl ${className}`}>
      <motion.div
        initial={{ opacity: 0 }}
        whileInView={{ opacity: 1 }}
        transition={{ delay: 0.5 }}
        className="lg:flex items-center  lg:gap-x-[129px] lg:mb-[80px]">
        <h1 className="text-3xl lg:text-5xl lg:leading-[81.6px] text-white mb-4 lg:mb-6 max-w-[470px]">
          What people are saying about us
        </h1>
        <p className="text-opacity-70 text-white max-w-[450px] mb-10 lg:mb-12 ml-auto">
          Everything you need to accept card payments and grow your business anywhere on the planet.
        </p>
      </motion.div>
      <motion.div
        initial={{ opacity: 0 }}
        whileInView={{ opacity: 1 }}
        transition={{ delay: 0.5 }}
        className="mb-32 lg:mb-[100px]">
        <OpinionList />
      </motion.div>
      <div className="flex flex-col lg:flex-row justify-center items-center gap-y-20 lg:gap-y-0 gap-x-[100px]">
        <motion.img
          initial={{ opacity: 0, y: 200 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{
            ease: 'easeOut',
            duration: 1,
            delay: 0.25,
            stiffness: 300,
            damping: 24
          }}
          exit={{ opacity: 0, y: 200 }}
          src={AirbnbImage}
          alt="Airbnb logo"
          className="max-w-[192.25px]"
        />
        <motion.img
          initial={{ opacity: 0, y: 200 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{
            ease: 'easeOut',
            duration: 1,
            delay: 0.5,
            stiffness: 300,
            damping: 24
          }}
          exit={{ opacity: 0, y: 200 }}
          src={BinanceImage}
          alt="Binance logo"
          className="max-w-[192.25px]"
        />
        <motion.img
          initial={{ opacity: 0, y: 200 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{
            ease: 'easeOut',
            duration: 1,
            delay: 0.75,
            stiffness: 300,
            damping: 24
          }}
          exit={{ opacity: 0, y: 200 }}
          src={CoinbaseImage}
          alt="Coinbase logo"
          className="max-w-[192.25px]"
        />
        <motion.img
          initial={{ opacity: 0, y: 200 }}
          whileInView={{ opacity: 1, y: 0 }}
          transition={{
            ease: 'easeOut',
            duration: 1,
            delay: 1,
            stiffness: 300,
            damping: 24
          }}
          exit={{ opacity: 0, y: 200 }}
          src={DropboxImage}
          alt="Dropbox logo"
          className="max-w-[192.25px]"
        />
      </div>
    </section>
  );
};

OpinionComponent.propTypes = {
  className: propTypes.string.isRequired
};
export default OpinionComponent;
