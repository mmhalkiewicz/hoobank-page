import React from 'react';
import propTypes from 'prop-types';

const DesktopMenu = ({ className }) => {
  return (
    <div className={className}>
      <ul className="flex gap-x-8 lg:gap-x-[56px] text-white">
        <li className="opacity-70 hover:opacity-100 cursor-pointer ease-in-out duration-300">
          Home
        </li>
        <li className="opacity-70 hover:opacity-100 cursor-pointer ease-in-out duration-300">
          About Us
        </li>
        <li className="opacity-70 hover:opacity-100 cursor-pointer ease-in-out duration-300">
          Features
        </li>
        <li className="opacity-70 hover:opacity-100 cursor-pointer ease-in-out duration-300">
          Solution
        </li>
      </ul>
    </div>
  );
};

DesktopMenu.propTypes = {
  className: propTypes.string.isRequired
};

export default DesktopMenu;
