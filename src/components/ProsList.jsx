import React from 'react';
import ProsItem from './ProsItem';
import propTypes from 'prop-types';
import { motion } from 'framer-motion';

const ProsList = ({ className }) => {
  const list = [
    {
      id: 1,
      number: '3800+',
      text: 'User Active'
    },
    {
      id: 2,
      number: '230+',
      text: 'TRUSTED BY COMPANY'
    },
    {
      id: 3,
      number: '$230M+',
      text: 'TRANSACTION'
    }
  ];

  return (
    <motion.div
      initial={{ opacity: 0, scale: 0.5 }}
      whileInView={{ opacity: 1 }}
      animate={{ scale: 1 }}
      transition={{ duration: 0.5 }}
      className={`flex flex-wrap mx-4  lg:justify-between md:mx-auto md:max-w-screen-md lg:max-w-screen-xl ${className}`}>
      <ProsItem list={list} />
    </motion.div>
  );
};

ProsList.propTypes = {
  className: propTypes.string
};

export default ProsList;
