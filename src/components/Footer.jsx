import React from 'react';
import LogoLarge from '../assets/logo-big.svg';
import LinkList from './LinkList';
import { AiOutlineInstagram } from 'react-icons/ai';
import { FaFacebook, FaTwitter, FaLinkedin } from 'react-icons/fa';

const Footer = () => {
  const linkListsList = [
    {
      id: 1,
      title: 'Useful Links',
      links: ['Content', 'How it Works', 'Create', 'Explore', 'Terms & Services']
    },
    {
      id: 2,
      title: 'Community',
      links: ['Help Center', 'Partners', 'Suggestions', 'Blog', 'Newsletters']
    },
    {
      id: 3,
      title: 'Partner',
      links: ['Our Partner', 'Become a Partner']
    }
  ];
  return (
    <footer className="footer-background ">
      <div className="mx-5  md:mx-auto md:max-w-screen-md lg:max-w-screen-xl ">
        <div className="lg:flex pt-10 lg:pt-[89.91px] lg:mb-[40px]">
          <div className="lg:mr-[210px]">
            <div className=" flex justify-center lg:justify-normal items-center  mb-10">
              <img src={LogoLarge} alt="Company's logo" className=" mr-[10px] " />
              <h1 className="text-[40.58px] font-bold">
                {' '}
                <span className="text-white">Hoo</span>
                <span className="text-[#5CE1E6]">Bank</span>
              </h1>
            </div>
            <p className="text-white text-opacity-70 lg:text-[18px] lg:leading-[32px] text-center lg:text-left mx-auto mb-10 max-w-[312px]">
              A new way to make the payments easy, reliable and secure.
            </p>
          </div>
          <div className="mb-10 lg:mb-0 lg:flex text-center lg:text-left">
            {linkListsList.map((linkList) => (
              <LinkList
                list={linkList}
                key={linkList.id}
                className={'lg:even:mr-[183px] lg:even:ml-[146px]'}
              />
            ))}
          </div>
        </div>
        <div className="border-t-2 border-[#3F3E45] lg:flex items-baseline pb-10">
          <div className="mt-[30px] mb-10 lg:mb-0">
            <p className="text-white text-center text-opacity-60 text-[18px]">
              Copyright &#169; 2021 HooBank. All Rights Reserved.
            </p>
          </div>
          <div className="text-white flex-row flex justify-around lg:ml-auto lg:gap-x-[30px]">
            <AiOutlineInstagram
              size={20}
              className="hover:scale-125 ease-in-out duration-500 cursor-pointer"
            />
            <FaFacebook
              size={20}
              className="hover:scale-125 ease-in-out duration-500 cursor-pointer"
            />
            <FaTwitter
              size={20}
              className="hover:scale-125 ease-in-out duration-500 cursor-pointer"
            />
            <FaLinkedin
              size={20}
              className="lg:mr-[30px] hover:scale-125 ease-in-out duration-500 cursor-pointer"
            />
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
