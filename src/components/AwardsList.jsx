import React from 'react';
import Star from '../assets/Star.svg';
import Shield from '../assets/Shield Done.svg';
import Send from '../assets/Send.svg';
import { motion } from 'framer-motion';

const AwardsList = () => {
  return (
    <ul className="flex flex-col gap-y-5">
      <motion.li
        initial={{ opacity: 0, y: 200 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{
          ease: 'easeOut',
          duration: 0.25,
          delay: 0.25,
          stiffness: 300,
          damping: 24
        }}
        exit={{ opacity: 0, y: 200 }}
        className="flex items-center justify-center  text-white black-gradient px-5 py-6 rounded-3xl cursor-pointer h-[115px] w-[470px]">
        <div className="bg-[#09977C1A] aspect-square  w-8 h-8 lg:w-[64px] lg:h-[64px]  rounded-full flex  items-center justify-center">
          <img src={Star} alt="Icon" className="w-1/4 " />
        </div>
        <div className=" ml-2 lg:mr-5">
          <h4 className="text-white font-bold lg:text-lg">Rewards</h4>
          <p className="text-white text-opacity-70 lg:text-md leading-[24px]">
            The best credit cards offer some tantalizing combinations of promotions and prizes.
          </p>
        </div>
      </motion.li>
      <motion.li
        initial={{ opacity: 0, y: 200 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{
          ease: 'easeOut',
          duration: 0.25,
          delay: 0.5,
          stiffness: 300,
          damping: 24
        }}
        exit={{ opacity: 0, y: 200 }}
        className="flex items-center justify-center  text-white black-gradient px-5 py-6 rounded-3xl cursor-pointer h-[115px] w-[470px]">
        <div className="bg-[#09977C1A] aspect-square  w-8 h-8 lg:w-[64px] lg:h-[64px]  rounded-full flex  items-center justify-center">
          <img src={Shield} alt="Icon" className="w-1/4 " />
        </div>
        <div className=" ml-2 lg:mr-5">
          <h4 className="text-white font-bold lg:text-lg">100% Secured</h4>
          <p className="text-white text-opacity-70 lg:text-md leading-[24px]">
            We take proactive steps make sure your information and transactions are secure.
          </p>
        </div>
      </motion.li>
      <motion.li
        initial={{ opacity: 0, y: 200 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{
          ease: 'easeOut',
          duration: 0.25,
          delay: 0.75,
          stiffness: 300,
          damping: 24
        }}
        exit={{ opacity: 0, y: 200 }}
        className="flex items-center justify-center  text-white black-gradient px-5 py-6 rounded-3xl cursor-pointer h-[115px] w-[470px]">
        <div className="bg-[#09977C1A] aspect-square  w-8 h-8 lg:w-[64px] lg:h-[64px]  rounded-full flex  items-center justify-center">
          <img src={Send} alt="Icon" className="w-1/4 " />
        </div>
        <div className=" ml-2 lg:mr-5">
          <h4 className="text-white font-bold lg:text-lg">Balance Transfer</h4>
          <p className="text-white text-opacity-70 lg:text-md leading-[24px]">
            A balance transfer credit card can save you a lot of money in interest charges.
          </p>
        </div>
      </motion.li>
      F
    </ul>
  );
};

export default AwardsList;
