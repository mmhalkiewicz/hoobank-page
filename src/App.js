import Navigation from './components/Navigation';
import Hero from './components/Hero';
import ProsList from './components/ProsList';
import HeroTwo from './components/HeroTwo';
import Illustration from './components/Illustration';
import HeroThird from './components/HeroThird';
import OpinionComponent from './components/OpinionComponent';
import NewsletterComponent from './components/NewsletterComponent';
import Footer from './components/Footer';

function App() {
  return (
    <div>
      <Navigation />
      <div className="lg:flex lg:flex-row lg:items-center md:mx-auto md:max-w-screen-md lg:max-w-screen-xl mt-20 lg:mt-0 mb-32 lg:mb-40  lg:mt-[70px]">
        <Hero />
        <Illustration className="hidden lg:block" />
      </div>
      <ProsList className="mb-32 lg:mb-48" />
      <HeroTwo className="mb-32" />
      <HeroThird className="mb-32 lg:mb-[224px]" />
      <OpinionComponent className="mb-32 lg:mb-[100px]" />
      <NewsletterComponent className="mb-32 lg:mb-[82px]" />
      <Footer />
    </div>
  );
}

export default App;
